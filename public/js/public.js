$(document).ready(function(){
	//iframe的高度更与屏幕的高度
	var winH = $(window).height() +"px" ; //浏览器当前窗口可视区域高度
	$(".myiframe").css({"height":winH});
	//频道管理顶部距离
	var headH = $(".headDiv").outerHeight();
	$(".channel").css({"top":headH});
	//点击导航添加触发样式
	$(".swiper-slide").on("touchstart",function(){
		$(this).addClass("swiper-slide-active").siblings().removeClass("swiper-slide-active");
	})
	/*
	//点击显示搜索框
	$(".sear-btn").on("touchstart",function(e){
		$(".swiper").hide();
		$(".search").fadeIn(600);
		$(".sear-btn").fadeOut(300);
		e.preventDefault();
	})
	//点击取消显示搜索框
	$(".cancel").on("touchstart ",function(e){		
		$(".search").hide();
		$(".sear-btn").fadeIn(300);
		$(".swiper").fadeIn(600);	
		e.preventDefault();
	})
	*/
	$(".mask").on("click touchstart",function(e){
		$(this).fadeOut(300);
		$(".district-list").animate({"left":"-69.6%"},500);
		$(".shareDiv").animate({"bottom":"-350px"},500);
		e.preventDefault();
	})
	//点击回到顶部按钮
	$(".topBtn-img").on("touchstart",function(){
		$("html,body").animate({"scrollTop":"0px"},300);
		return false;
	})
	//文章详情页点击更多分享按钮
	$(".gdBtn").on("touchstart",function(e){
		$(".shareDiv").animate({"bottom":"0px"},500);
		$(".mask").fadeIn(300);	
		e.preventDefault();			
	})
	$(".cancelBtn").on("touchstart",function(e){
		$(".shareDiv").animate({"bottom":"-350px"},500);
		$(".mask").fadeOut(300);
		e.preventDefault();
	})
	//文章详情页点赞功能
	$(document).ready(function(e) {
		$('.zhan').one("click",function(){
			var left = parseInt($(this).offset().left)+10, top = parseInt($(this).offset().top)-10, obj=$(this);
			$(this).append('<div id="zhan"><b>+1<\/b></\div>');
			$('#zhan').css({'position':'absolute','z-index':'1', 'color':'#C30','left':left+'px','top':top+'px'}).animate({top:top-10,left:left+10},'slow',function(){
				$(this).fadeIn('fast').remove();
				var Num = parseInt(obj.find('span').text());
				Num++;
				obj.find('span').text(Num);
			});
			$(this).children('img').attr('src','img/index_24.png');
			return false;
		});
	});
	//文章详情页弹出评论框
	/*
	$(".disInput").on("touchstart",function(e){
		$(".fabu").stop().slideDown();
		$(".topBtn").stop().hide();
		$(".discuss").stop().hide();
		e.preventDefault();
	})
	*/
	/*
	$(".cancelPl").on("touchstart",function(e){
		$(".fabu").stop().slideUp();
		$(".topBtn").stop().show();
		$(".discuss").stop().show();
		e.preventDefault();
	})
	*/
	/*
	$(".publish").on("touchstart",function(e){
		$(".fabu-nr").val();
		$(".fabu").stop().slideUp();
		$(".discuss").stop().show();
		alert($(".fabu-nr").val());
		e.preventDefault();
	})
	*/
	/*
	$(".liuyan").on("touchstart",function(e){
		$(".fabu").stop().slideDown();
		$(".topBtn").stop().hide();
		$(".discuss").stop().hide();
		e.preventDefault();
	})
	*/
	//专题页全部展开功能
	$(".introZt").each(function(){
        var maxwidth=60;//设置最多显示的字数
        var text=$(this).text();
        if($(this).text().length>maxwidth){
            $(this).text($(this).text().substring(0,maxwidth));
            $(this).html($(this).html()+"..."+"<a href='###'>全部展开+</a>");//如果字数超过最大字数，超出部分用...代替，并且在后面加上点击展开的链接；			
        };
        $(this).find("a").click(function(){
            $(this).parent().text(text);//点击“点击展开”，展开全文
        })
    })
})

//回到顶部
$(window).scroll(function(){
	var scrollTop = $(window).scrollTop();
	if(scrollTop > 200){	
		$(".topBtn").stop().show(200);		
	}else{
		$(".topBtn").stop().hide(200);		
	}	
})
//登录页
$(function(){
	setInterval(function(){
		$(".login ul li:nth-child(1)").animate({"padding-left":"60px"},300,function(){
			$(".login ul li:nth-child(1)").animate({"padding-left":"0px"},300);
			$(".login ul li:nth-child(2)").animate({"padding-left":"65px"},320,function(){
				$(".login ul li:nth-child(2)").animate({"padding-left":"0px"},320);
				$(".login ul li:nth-child(3)").animate({"padding-left":"70px"},400,function(){
					$(".login ul li:nth-child(3)").animate({"padding-left":"0px"},400);
					$(".login ul li:nth-child(4)").animate({"padding-left":"90px"},450,function(){
						$(".login ul li:nth-child(4)").animate({"padding-left":"0px"},450);
						});
					});
				});
			});
		},5000)
	})	
