/*
 * api接口定义
 */

import { CALL_API, Schemas } from '../middleware/api'
import { get_endpoint_topic, get_endpoint_photo, get_endpoint_user_profile, 
	API_GET_COMMENT, API_SEARCH
} from '../conf/apis'

// 更新swiper
export const UPDATE_SWIPER_INSTANCE = 'UPDATE_SWIPER_INSTANCE'

// 初始化swiper
export const initSwiper = () => (dispatch, getState) => {
	const action = {
		type: UPDATE_SWIPER_INSTANCE
	}
	return dispatch(action)
}


export const POSTS_REQUEST = 'POSTS_REQUEST'
export const POSTS_SUCCESS = 'POSTS_SUCCESS'
export const POSTS_FAILURE = 'POSTS_FAILURE'

// Fetches posts from api
// Relies on the custom API middleware defined in ../middleware/api.js.
const fetchPosts = (category, nextPageUrl) => ({
	posts: 'category-' + category,
  [CALL_API]: {
    types: [ POSTS_REQUEST, POSTS_SUCCESS, POSTS_FAILURE ],
    endpoint: nextPageUrl,
    schema: Schemas.POST_ARRAY
  }
})

// Fetch posts: a list of post
// Relies on Redux Thunk middleware.
export const loadPosts = (category, nextPageUrl) => (dispatch, getState) => {
  return dispatch(fetchPosts(category, nextPageUrl))
}

// Fetch post
export const POST_REQUEST = 'POST_REQUEST'
export const POST_SUCCESS = 'POST_SUCCESS'
export const POST_FAILURE = 'POST_FAILURE'

// Fetches post: one post
// Relies on the custom API middleware defined in ../middleware/api.js.
const fetchPost = (id) => ({
	post: id,
	[CALL_API]: {
		types: [ POST_REQUEST, POST_SUCCESS, POST_FAILURE ],
		endpoint: `/post?id=${id}`,
		schema: Schemas.POST
	}
})
export const loadPost = (id) => (dispatch, getState) => {
	return dispatch(fetchPost(id))
}

export const RELATED_POST_REQUEST = 'RELATED_POST_REQUEST'
export const RELATED_POST_SUCCESS = 'RELATED_POST_SUCCESS'
export const RELATED_POST_FAILURE = 'RELATED_POST_FAILURE'

// Fetch the related keys
const fetchRelated = (id) => ({
	post: 'post-' + id,
  [CALL_API]: {
    types: [ RELATED_POST_REQUEST, RELATED_POST_SUCCESS, RELATED_POST_FAILURE ],
    endpoint: `/related?id=${id}`,
    schema: Schemas.POST_ARRAY
  }
})

export const loadRelated = (id) => (dispatch, getState) => {
	return dispatch(fetchRelated(id))
}

export const TOPICS_REQUEST = 'TOPICS_REQUEST'
export const TOPICS_SUCCESS = 'TOPICS_SUCCESS'
export const TOPICS_FAILURE = 'TOPICS_FAILURE'

const fetchTopics = (nextPageUrl) => ({
	topics: 'topics',
	[CALL_API]: {
		types: [ TOPICS_REQUEST, TOPICS_SUCCESS, TOPICS_FAILURE ],
		endpoint: nextPageUrl,
		schema: Schemas.TOPIC_ARRAY
	}
})

export const TOPIC_REQUEST = 'TOPIC_REQUEST'
export const TOPIC_SUCCESS = 'TOPIC_SUCCESS'
export const TOPIC_FAILURE = 'TOPIC_FAILURE'

export const loadTopics = (nextPageUrl) => (dispatch, getState) => {
	return dispatch(fetchTopics(nextPageUrl))
}

const fetchTopic = (id) => ({
	key: 'topic-' + id,
	[CALL_API]: {
		types: [ TOPIC_REQUEST, TOPIC_SUCCESS, TOPIC_FAILURE ],
		endpoint: get_endpoint_topic(id),
		schema: Schemas.TOPIC_DETAIL
	}

})

export const loadTopic = (id) => (dispatch, getState) => {
	return dispatch(fetchTopic(id))
}

export const PHOTOS_REQUEST = 'PHOTOS_REQUEST'
export const PHOTOS_SUCCESS = 'PHOTOS_SUCCESS'
export const PHOTOS_FAILURE = 'PHOTOS_FAILURE'


const fetchPhotos = (nextPageUrl) => ({
	key: 'photos',
	[CALL_API]: {
		types: [ PHOTOS_REQUEST, PHOTOS_SUCCESS, PHOTOS_FAILURE ],
		endpoint: nextPageUrl,
		schema: Schemas.PHOTO_ARRAY
	}

})

export const loadPhotos = (nextPageUrl) => (dispatch, getState) => {
	return dispatch(fetchPhotos(nextPageUrl))
}

export const PHOTO_REQUEST = 'PHOTO_REQUEST'
export const PHOTO_SUCCESS = 'PHOTO_SUCCESS'
export const PHOTO_FAILURE = 'PHOTO_FAILURE'


const fetchPhoto = (id) => ({
	key: 'photos',
	[CALL_API]: {
		types: [ PHOTO_REQUEST, PHOTO_SUCCESS, PHOTO_FAILURE ],
		endpoint: get_endpoint_photo(id),
		schema: Schemas.PHOTO
	}

})

export const loadPhoto = (id) => (dispatch, getState) => {
	return dispatch(fetchPhoto(id))
}


// load user profile
export const USER_REQUEST = 'USER_REQUEST'
export const USER_SUCCESS = 'USER_SUCCESS'
export const USER_FAILURE = 'USER_FAILURE'

export const loadUserProfile = () => (dispatch, getState) => {
	return dispatch({
		key: 'user',
		 [CALL_API]: {
			 types: [ USER_REQUEST, USER_SUCCESS, USER_FAILURE ],
			 endpoint: get_endpoint_user_profile(),
			 schema: Schemas.USER
		 }
	})
}

export const UPDATE_USER = 'UPDATE_USER'

export const updateUser = (query) => (dispatch, getState) => {
	return dispatch({
		type: 'UPDATE_USER',
		user: query
	})
} 

export const UPDATE_KEYWORD = 'UPDATE_KEYWORD'

export const updateKeyword = (keyword) => (dispatch, getState) => {
	return dispatch({
		type: UPDATE_KEYWORD,
		keyword: keyword
	})
} 

export const COMMENT_REQUEST = 'COMMENT_REQUEST'
export const COMMENT_SUCCESS = 'COMMENT_SUCCESS'
export const COMMENT_FAILURE = 'COMMENT_FAILURE'

export const loadComments = (id, lastCommentId = null, parentCommentId = null) => (dispatch, getState) => {
	let commentAPI = API_GET_COMMENT + "?attachId=" + id
	if(lastCommentId) {
		commentAPI += `&cursor=${lastCommentId}`
	}
	if(parentCommentId) {
		commentAPI += `&parentId=${parentCommentId}`
	}
	return dispatch({
		key: 'post-' + id,
		 [CALL_API]: {
			 types: [ COMMENT_REQUEST, COMMENT_SUCCESS, COMMENT_FAILURE ],
			 endpoint: commentAPI,
			 schema: Schemas.COMMENT_ARRAY
		 }

	})
}

export const SEARCH_REQUEST = 'SEARCH_REQUEST'
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS'
export const SEARCH_FAILURE = 'SEARCH_FAILURE'

/**
 * search action
 */
export const doSearch = (keyword, nextPageUrl) => (dispatch, getState) => {
	return dispatch({
		key: 'search-' + keyword,
		 [CALL_API]: {
			types: [ SEARCH_REQUEST, SEARCH_SUCCESS, SEARCH_FAILURE ],
			endpoint: nextPageUrl,
			schema: Schemas.POST_ARRAY
		 }

	})
}
