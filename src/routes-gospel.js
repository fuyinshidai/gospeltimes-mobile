import React from 'react'
import { Route, IndexRoute } from 'react-router'
import GospelApp from './containers/GospelApp'
import IndexPage from './containers/IndexPage'
import Topic from './containers/Topic'
import Topics from './containers/Topics'
import Photos from './containers/Photos'
import Photo from './containers/Photo'
import Search from './containers/Search'
import PostDetailPage from './containers/PostDetailPage'

export default <Route path="/" component={GospelApp}>
	<IndexRoute component={IndexPage}/>
  <Route path="/news/:id" component={PostDetailPage} />
  <Route path="/category/:id" component={IndexPage} />
  <Route path="/province/:id" component={IndexPage} />
  <Route path="/topics" component={Topics} />
  <Route path="/topic/:id" component={Topic} />
  <Route path="/photos" component={Photos} />
  <Route path="/photo/:id" component={Photo} />
  <Route path="/search" component={Search} />
</Route>
