/*
 * 专题详情页面
 * 通过url获取专题的id
 * 根据id和缓存检查是否需要重新从远程载入专题
 * 对获取的专题进行显示
 */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { loadTopic } from '../actions/peniel'
import { SOURCE_SITE_ROOT } from '../conf/apis'


import DocumentTitle from 'react-document-title'

import { loading } from '../utils/helper'
import PostList from '../components/PostList'
import PostTeaser from '../components/PostTeaser'
import PostTeaserMoreImgs from '../components/PostTeaserMoreImgs'


class Topic extends Component {
	static propTypes = {
		loadTopic: PropTypes.func.isRequired,
		topicStatus: PropTypes.object.isRequired
	}

	// 第一次打开页面手动输入url
  componentWillMount() {
		// 加载专题
		const { loadTopic, topic } = this.props
		const topicId = this.props.params.id
		if(typeof topic.topic === 'undefined') {
			loadTopic(topicId)
		}
  }

	// 显示列表
  renderPostTeaser(post, index) {
		if(!post) {
			return
		}
		// 如果是第一个文章显示大图
		let teaser
		const imgsCount = post.imgs.length
		if(imgsCount > 1) {
			teaser = <PostTeaserMoreImgs
				index={index}
				post={post}
				key={post.id}
				text={post.text}
			/>
		} else {
			teaser = <PostTeaser
				index={index}
				post={post}
				key={post.id}
				text={post.text}
			/>
		}

    return teaser
  }

	render() {
		const { topic, topicStatus } = this.props
		const detail = topic.topic || {}
		const items = topic.news || []
		const smeta = detail.smeta || '{}'
		const smetaObj = JSON.parse(smeta)
		const src = (smetaObj.big_pic && SOURCE_SITE_ROOT + smetaObj.big_pic) || false
		const img = (smetaObj.big_pic && <img src={src} alt="" />) || null
		return (
		<DocumentTitle title='专题-福音时报'>
			<div className="main inner">
				{topicStatus.isFetching ? loading : ''}
				<div className="lists">
					<article className="clearfix">
							<div className="news-list clearfix">
										<div className="news-imga imgaZt clearfix">
										{img}
										</div>
										<h1 className="news-h1">{detail.title}</h1>
								<div className="introZt">
									<div dangerouslySetInnerHTML={{__html: detail.content}}></div>
								</div>
							</div> 
					 </article>
				</div>
				<PostList 
					items={items}
					renderItem={this.renderPostTeaser}
				/>
			</div>			
		</DocumentTitle>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
	const id = ownProps.params.id
  const {
		pagination,
		entities: { topicDetails }
  } = state

	const topicStatus = (pagination['topicList']['topic-' + id]) || {}
	const topic = (topicDetails && topicDetails[id]) || {}

	return {
		topic,
		topicStatus
	}
}

export default connect(mapStateToProps, {
	loadTopic
})(Topic)
