/*
 * 搜索页面
 */
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { doSearch } from '../actions/peniel'
import DocumentTitle from 'react-document-title'

import PostList from '../components/PostList'
import PostTeaser from '../components/PostTeaser'
import PostTeaserMoreImgs from '../components/PostTeaserMoreImgs'
import PostTeaserBigImage from '../components/PostTeaserBigImage'
import { loading } from '../utils/helper'

import Waypoint from 'react-waypoint'

class Search extends Component {

	static propTypes = {
		keyword: PropTypes.string,
		postsList: PropTypes.array.isRequired,
		postsPagination: PropTypes.object.isRequired,
		doSearch: PropTypes.func.isRequired
	}

	// 当页面滚动到底部的时候，加载更多新闻
	_handleWaypointEnter() {
		// trigger load more
		const { isFetching, doSearch, keyword, nextPageUrl } = this.props
		// 如果没有载入，载入中显示
		if(isFetching === false) {
			doSearch(keyword, nextPageUrl)
		}
	}

	// 显示列表
  renderPostTeaser(post, index) {
		// 如果是第一个文章显示大图
		let teaser
		if(index === 0) {
			teaser = <PostTeaserBigImage
				index={index}
				post={post}
				key={post.id}
      />
		} else {
			const imgsCount = post.imgs.length
			if(imgsCount > 1) {
				teaser = <PostTeaserMoreImgs
					index={index}
					post={post}
					key={post.id}
					text={post.text}
				/>
			} else {
				teaser = <PostTeaser
					index={index}
					post={post}
					key={post.id}
					text={post.text}
				/>
			}
		}

    return teaser
  }

	render() {
		const { keyword, postsList, postsPagination, doSearch } = this.props
		let keyW = keyword || ''
		const pageTitype = `搜索 ${keyW}-福音时报`

		return (
		<DocumentTitle title={pageTitype}>
			<div className="main inner">
				<PostList 
					items={postsList}
					renderItem={this.renderPostTeaser}
				/>
				<Waypoint
					onEnter={this._handleWaypointEnter}
					doSearch={doSearch}
					keyword={keyword}
					{...postsPagination}
				/>
				{postsPagination.isFetching ? loading : ''}
			</div>			
		</DocumentTitle>
			   )
	}
}

const mapStateToProps = (state, ownProps) => {
	const {
		pagination: { pagedSearch },
		entities: { posts },
	} = state

	const keyword = state.entities.keyword;
	const pagedKey = 'search-' + keyword
	const postsPagination = pagedSearch[pagedKey] || {ids: []}
	const postsList = postsPagination.ids.map(id => posts[id])

	return {
		keyword,
		postsList,
		postsPagination
	}
}

export default connect(mapStateToProps, {
	doSearch
})(Search)
