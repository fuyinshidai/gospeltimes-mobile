/*
 * 图片详情页面
 * 通过url获取专题的id
 * 根据id和缓存检查是否需要重新从远程载入专题
 * 对获取的专题进行显示
 */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { loadPhoto, loadPhotos } from '../actions/peniel'
import PhotoItems from '../components/PhotoItems'


import DocumentTitle from 'react-document-title'
import PhotoList from '../components/PhotoList'
import PhotoTeaser from '../components/PhotoTeaser' 

// import { addComment } from '../utils/helper'

import $ from 'jquery'


class Photo extends Component {
	static propTypes = {
		loadPhotos: PropTypes.func.isRequired,
		photoList: PropTypes.array.isRequired,
		photo: PropTypes.object.isRequired,
		loadPhoto: PropTypes.func.isRequired
	}

	// 更新了url
  componentWillReceiveProps(nextProps) {

		// 如果更新了url
		if (this.props.location !== nextProps.location) {
			$("html, body").animate({"scrollTop":"0px"},500);
		}
  }

	// 第一次打开页面手动输入url
  componentWillMount() {
		// 加载专题
		const { photoList, photo, loadPhoto, params, loadPhotos } = this.props
		if(typeof photo.id === 'undefined') {
			loadPhoto(params.id)
		}
		if (photoList.length === 0) {
			loadPhotos('/photos?recommend=1')
		}
  }

	componentDidMount() {
			$("html, body").animate({"scrollTop":"0px"},500);
			// addComment()
  }

	renderPhotoImg(photo, index) {
		return <figure key={index}> 
						<a href={photo.url} data-size="690x406"><img src={photo.url} alt={photo.alt} /></a>
						<figcaption>{photo.alt}</figcaption>
					</figure>
	}

	// 显示列表
  renderTeaser(item, index) {
		return <PhotoTeaser
						index={index}
						item={item}
						key={item.id}
						/>
  }



	render() {
		const { photo, photoList } = this.props
		const smeta = photo.smeta || '{}'
		const smetaObj = JSON.parse(smeta)
		const items = smetaObj.photo || []
		const title = photo.title + '-福音时报'
		const sid = 'photo-' + photo.id
		return (
		<DocumentTitle title={title}>
			<div className="main inner">
				<div className="headline container clearfix">
					<h1>{ photo.title }</h1>
					<div className="data clearfix">
						<span>作者：{ photo.author }</span>
						<span>{ photo.date }</span>
					</div>
				</div>
				<PhotoItems 
					items={items}
					renderItem={this.renderPhotoImg}
				/>
				<div id="SOHUCS" sid={sid} ></div>

			<div className="comment comment-jc container clearfix">
				<div className="com-h1 red-h clearfix">
					<i></i>
					<h1>精彩推荐</h1>
				</div>
				<PhotoList
					items={photoList}
					renderItem={this.renderTeaser}
				/>
			</div>
			</div>			
		</DocumentTitle>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
	const id = ownProps.params.id
  const {
    pagination: { pagedPhotos },
		entities: { photos }
  } = state

	const photo = (photos && photos[id]) || {}
	const photosPagination = pagedPhotos['photos'] || {ids: []}
  const photoList = photosPagination.ids.map(id => photos[id])

	return {
		photoList,
		photo
	}
}

export default connect(mapStateToProps, {
	loadPhoto,
	loadPhotos
})(Photo)
