/*
 * 专题列表页面
 */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { loadTopics } from '../actions/peniel'
import { getEndPointByLocation } from '../conf/apis'

import TopicList from '../components/TopicList'
import TopicTeaser from '../components/TopicTeaser' 
import { GLOBAL_TITLE } from '../conf/apis'

import Waypoint from 'react-waypoint'
import DocumentTitle from 'react-document-title'

import { loading } from '../utils/helper'


class Topics extends Component {
	static propTypes = {
		topicList: PropTypes.array.isRequired,
		topicsPagination: PropTypes.object.isRequired,
		loadTopics: PropTypes.func.isRequired
	}

	// 第一次打开页面手动输入url
  componentWillMount() {
		const {topicList} = this.props
		if (topicList.length === 0) {
			this.props.loadTopics(getEndPointByLocation(this.props.location))
		}
  }

	// 更新了url
  componentWillReceiveProps(nextProps) {
  }

	// 当页面滚动到底部的时候，加载更多
	_handleWaypointEnter() {
		const { loadTopics, topicsPagination } = this.props
		loadTopics(topicsPagination.nextPageUrl)
	}

	// 显示列表
  renderTeaser(item, index) {
		return <TopicTeaser
						index={index}
						item={item}
						key={item.id}
						/>
  }

	render() {
		const { topicList, loadTopics, topicsPagination } = this.props
		const title = '专题-' + GLOBAL_TITLE
		return (
		<DocumentTitle title={title}>
			<div className="main inner">
				<TopicList 
					items={topicList}
					renderItem={this.renderTeaser}
				/>
				<Waypoint
					onEnter={this._handleWaypointEnter}
					loadTopics={loadTopics}
					topicsPagination={topicsPagination}
				/>
				{topicsPagination.isFetching ? loading : ''}
			</div>			
		</DocumentTitle>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
  const {
    pagination: { pagedTopics },
    entities: { topics },
  } = state

	const topicsPagination = pagedTopics['topics'] || {ids: []}
  const topicList = topicsPagination.ids.map(id => topics[id])

	return {
		topicList,
		topicsPagination
	}
}

export default connect(mapStateToProps, {
	loadTopics
})(Topics)
