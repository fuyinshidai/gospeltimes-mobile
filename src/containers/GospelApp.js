import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { resetErrorMessage } from '../actions'
import { initSwiper, updateUser, doSearch, updateKeyword } from '../actions/peniel'
import { Link, IndexLink } from 'react-router'
import cookie from 'cookies-js'
import { LOGIN_EXPIRE, getEndPointByLocation } from '../conf/apis'
import { browserHistory } from 'react-router';

import $ from 'jquery'

class GospelApp extends Component {
	static propTypes = {
		errorMessage: PropTypes.string,
		resetErrorMessage: PropTypes.func.isRequired,
		children: PropTypes.node,
		initSwiper: PropTypes.func.isRequired,
		updateUser: PropTypes.func.isRequired,
		doSearch: PropTypes.func.isRequired
	}

  constructor(props) {
    super(props)

	this.searchStart = this.searchStart.bind(this)
	this.searchHide = this.searchHide.bind(this)

  }

	componentWillMount() {
		// 检查和旧的版本的兼容
		const currentUrl = window.location.href
		const found = currentUrl.match(/#\/post\/(\d*)/)
		if(found) {
			const postId = found[1];
			browserHistory.push(`/news/${postId}`)
		}
	}

	componentDidMount() {
		// 初始化导航的滑动
		this.props.initSwiper()

		// 检查是否有用户登录
		// 如果用户登录，设置用户的登录状态
		const query = this.props.location.query
		const { updateUser } = this.props
		// 从cookie中读取用户账号
		let cookieUser = cookie.get('user')
		if(typeof cookieUser !== 'undefined') {
			cookieUser = JSON.parse(cookieUser)
			updateUser(cookieUser)
		}
		if(typeof query.loggedIn !== 'undefined') {
			// 根据url更新用户账号
			cookie.set('user', JSON.stringify(query), {expires: LOGIN_EXPIRE})
			updateUser(query)
		}
	}

	channelShow() {
		$(".channel").fadeIn(200);
	}

	channelHide() {
		$(".channel").fadeOut(200);
	}

	/**
	 * 显示地区导航
	 */
	handleDistrict = e => {
		$(".district-list").animate({"left":"0"},500)
		$(".mask").fadeIn(300)
		e.preventDefault()
	}

	handleDistrictHide = e => {
		$(".mask").fadeOut(300)
		$(".district-list").animate({"left":"-69.6%"},500);
		$(".shareDiv").animate({"bottom":"-350px"},500);
	}

	searchStart = e => {
		browserHistory.push('/search')
		$(".swiper").hide()
		$(".search").fadeIn(600)
		$(".sear-btn").fadeOut(300)
		e.preventDefault()
	}

	searchHide = e => {
		window.history.back(1);
		$(".search").hide()
		$(".sear-btn").fadeIn(300)
		$(".swiper").fadeIn(600)	
		e.preventDefault()
	}

	searching = e => {
		let keyword = e.target.value 
		const { doSearch, pagedSearch, location, updateKeyword } = this.props
		const nextPageUrl = pagedSearch.nextPageUrl || getEndPointByLocation(location) + keyword
		updateKeyword(keyword)
		doSearch(keyword, nextPageUrl)
	 }


	handleDismissClick = e => {
		this.props.resetErrorMessage()
		e.preventDefault()
	}

	renderErrorMessage() {
		const { errorMessage } = this.props
		if (!errorMessage){
			return null
		}

		return (
			<p style={{ background: '#e99', padding: 10 }}>
				<b>{errorMessage}</b>
				{' '}
				(<a href="#"
						onClick={this.handleDismissClick}>
						Dismiss-忽略
				</a>)
			</p>
		)
	}

	render() {
		const { children } = this.props
		let pageName = "" // 当前的页面类型

		const currentUrl = window.location.href
		const inNews = currentUrl.match(/\/news\/(\d*)/)
		if(inNews) {
			pageName = "news-detail"
		} else {
			pageName = "not-news"
		}
		return (
			<div className={pageName}>
			{/*head*/}
		<div className="headDiv clearfix ">
			{/*header*/}
			<header className="head-top clearfix">
				<div className="container head-top-con">
					<div className="district tag fl" onClick={this.handleDistrict}>
						<span>地区</span>
					</div>
					<div className="logo"><Link to="/">
						<img src="/img/index_34.png" alt=""/>
					</Link></div>
					<div className="sear-btn fr" onClick={this.searchStart}><a href="#">
						<img src="/img/index_1_03.png" alt="" />
					</a></div>
				</div>
			</header>
			{/*nav*/}
			<nav className="nav clearfix">	
				{/*搜索框*/}
				<div className="search container clearfix">
					<div className="searInut fl">
						<form action="search.html" method="get">
							<input type="text" className="searText" onChange={this.searching} placeholder="基督教" />
							<input type="submit" className="searSub" value="" />
						</form>
					</div>
					<div className="cancel fr" onClick={this.searchHide}>取消</div>
				</div>
				<div className="swiper">
					{/* Swiper*/} 
				    <div className="swiper-container gallery-thumbs swiper-container-horizontal swiper-nav fl">
				        <div className="swiper-wrapper clearfix">
				            <div className="swiper-slide"><IndexLink to="/" activeClassName="active">首页</IndexLink></div>
				            <div className="swiper-slide"><Link to="/category/1" activeClassName="active">教会</Link></div>
				            <div className="swiper-slide"><Link to="/category/50" activeClassName="active">事工</Link></div>
				            <div className="swiper-slide"><Link to="/category/57" activeClassName="active">神学</Link></div>
				            <div className="swiper-slide"><Link to="/topics" activeClassName="active">专题</Link></div>
				            <div className="swiper-slide"><Link to="/category/17" activeClassName="active">社会</Link></div>
				            <div className="swiper-slide"><Link to="/category/30" activeClassName="active">文化</Link></div>
				        </div>
				    </div>			    
				    <div className="channel-btn fr" onClick={this.channelShow}><img src="/img/index_1_04.png" alt=""/></div>
			   </div>
			</nav>
		</div>
		{/*head end*/}

		{/*地区省份列表*/}
		<div className="district-list">
			<div className="districtBol">
				<i></i>
				<span>地区版</span>	
			</div>
			<ul className="districtUl">
				<li className="select"><Link to="/" onClick={this.handleDistrictHide}>全部</Link></li>
				<li><Link to='/province/42' onClick={this.handleDistrictHide}>北京</Link></li>
				<li><Link to='/province/145' onClick={this.handleDistrictHide}>上海</Link></li>
				<li><Link to='/province/59' onClick={this.handleDistrictHide}>天津</Link></li>
				<li><Link to='/province/76' onClick={this.handleDistrictHide}>河北</Link></li>
				<li><Link to='/province/88' onClick={this.handleDistrictHide}>内蒙古</Link></li>
				<li><Link to='/province/101' onClick={this.handleDistrictHide}>山西</Link></li>
				<li><Link to='/province/113' onClick={this.handleDistrictHide}>山东</Link></li>
				<li><Link to='/province/131' onClick={this.handleDistrictHide}>江苏</Link></li>
				<li><Link to='/province/164' onClick={this.handleDistrictHide}>安徽</Link></li>
				<li><Link to='/province/182' onClick={this.handleDistrictHide}>浙江</Link></li>
				<li><Link to='/province/194' onClick={this.handleDistrictHide}>福建</Link></li>
				<li><Link to='/province/204' onClick={this.handleDistrictHide}>江西</Link></li>
				<li><Link to='/province/216' onClick={this.handleDistrictHide}>黑龙江</Link></li>
				<li><Link to='/province/230' onClick={this.handleDistrictHide}>吉林</Link></li>
				<li><Link to='/province/240' onClick={this.handleDistrictHide}>辽宁</Link></li>
				<li><Link to='/province/255' onClick={this.handleDistrictHide}>河南</Link></li>
				<li><Link to='/province/274' onClick={this.handleDistrictHide}>广东</Link></li>
				<li><Link to='/province/296' onClick={this.handleDistrictHide}>广西</Link></li>
				<li><Link to='/province/311' onClick={this.handleDistrictHide}>湖北</Link></li>
				<li><Link to='/province/329' onClick={this.handleDistrictHide}>湖南</Link></li>
				<li><Link to='/province/344' onClick={this.handleDistrictHide}>海南</Link></li>
				<li><Link to='/province/347' onClick={this.handleDistrictHide}>云南</Link></li>
				<li><Link to='/province/364' onClick={this.handleDistrictHide}>贵州</Link></li>
				<li><Link to='/province/373' onClick={this.handleDistrictHide}>四川</Link></li>
				<li><Link to='/province/395' onClick={this.handleDistrictHide}>重庆</Link></li>
				<li><Link to='/province/435' onClick={this.handleDistrictHide}>陕西</Link></li>
				<li><Link to='/province/446' onClick={this.handleDistrictHide}>甘肃</Link></li>
				<li><Link to='/province/461' onClick={this.handleDistrictHide}>宁夏</Link></li>
				<li><Link to='/province/467' onClick={this.handleDistrictHide}>新疆</Link></li>
				<li><Link to='/province/482' onClick={this.handleDistrictHide}>青海</Link></li>
				<li><Link to='/province/491' onClick={this.handleDistrictHide}>西藏</Link></li>
				<li><Link to='/province/501' onClick={this.handleDistrictHide}>台湾</Link></li>
				<li><Link to='/province/499' onClick={this.handleDistrictHide}>香港</Link></li>
				<li><Link to='/province/500' onClick={this.handleDistrictHide}>澳门</Link></li>
			</ul>
		</div>
		{/*地区省份列表 end*/}

		{/*频道管理*/}
		<div className="channel">
			<div className="channel-title" onClick={this.channelHide}>
				<h2>频道管理</h2>
			</div>
			<div className="channel-list clearfix">
				<h3>我目前的频道</h3>
				<div className="classify nowBlock">
					<div><Link onClick={this.channelHide}  to="/">首页</Link></div>
					<span><Link onClick={this.channelHide} to="/category/1">教会</Link></span>
					<span><Link onClick={this.channelHide} to="/category/50">事工</Link></span>
					<span><Link onClick={this.channelHide} to="/category/57">神学</Link></span>
					<span><Link onClick={this.channelHide} to="/topics">专题</Link></span>
					<span><Link onClick={this.channelHide} to="/photos">图片</Link></span>
					<span><Link onClick={this.channelHide} to="/category/17">社会</Link></span>
					<span><Link onClick={this.channelHide} to="/category/30">文化</Link></span>
					<span><Link onClick={this.channelHide} to="/category/39">家庭</Link></span>
					<span><Link onClick={this.channelHide} to="/category/75">商业</Link></span>
					<span><Link onClick={this.channelHide} to="/category/25">国际</Link></span>
				</div>
			</div>
		</div>
		{/*频道管理 end*/}

		{/*返回顶部*/}
	    <div className="topBtn">
			<img className="topBtn-img" src="/img/index_26.png" alt="go to top" />
			<Link to="/"><img src="/img/home.png" alt="go to home"/></Link>
		</div>
		{/*返回顶部 end*/}

			{/*遮罩层*/}
		<div className="mask"></div>
			{/*遮罩层 end*/}
				{this.renderErrorMessage()}
				{children}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
	const {
		pagination: { pagedSearch },
	} = state
	
	return {
		errorMessage: state.errorMessage,
		pagedSearch
	}
}

export default connect(mapStateToProps, {
	resetErrorMessage,
	initSwiper,
	updateUser,
	doSearch,
	updateKeyword
})(GospelApp)
