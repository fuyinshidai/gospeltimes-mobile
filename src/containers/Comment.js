/*
 * 根据用户是否已经登录显示显示评论框
 */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import $ from 'jquery'
import Login from '../containers/Login'

class Comment extends Component {
	static propTypes = {
		user: PropTypes.object.isRequired,
		showComment: PropTypes.func.isRequired
	}

  constructor() {
    super();
    this.state = {
      showLogin: false
    }
  }

	// 更新了url
  componentWillReceiveProps(nextProps) {
  }

	componentDidMount() {
  }

	onFocus(thisCon) {
		const { user } = this.props
		// 如果用户已经登录，弹出发表评论框
		// 如果用户没有登录，显示用户登录框
		// 检查用户是否已经登录
		if (typeof(user.id) === 'undefined') {
			// first fetch remote user profile page
			// if user profile exists make the user loggin
			// else popup the log in page
			// loadUserProfile()
			$("#loginForm").show()
		} else {
			thisCon.props.showComment()
		}

	}

	render() {
		const showStatus = { display: this.state.showLogin ? 'block' : 'none' }
		const { user } = this.props
		const avatar = user.avatar ? user.avatar : "/img/avatar.png"
		return (
			<div>
				<div className="discuss clearfix">
					<div className="container clearfix">
						<div className="clearfix">
							<div className="head fl">
								<img src={avatar} alt="avatar" />
							</div>

							<div className="disInput fl">
								<textarea onTouchStart={(evt) => this.onFocus(this)} disabled="disabled" type="text" placeholder="添加评论" value="添加评论"></textarea>
							</div>

						</div>
					</div>
				</div> 	  

				<Login />

		</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
  const {
		entities: { user }
  } = state
	return {
		user
	}
}

export default connect(mapStateToProps, {
})(Comment)
