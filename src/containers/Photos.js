/*
 * 图片
 */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { loadPhotos } from '../actions/peniel'
import { GLOBAL_TITLE } from '../conf/apis'

import DocumentTitle from 'react-document-title'
import Waypoint from 'react-waypoint'

import PhotoList from '../components/PhotoList'
import PhotoTeaser from '../components/PhotoTeaser' 
import { loading } from '../utils/helper'

class Photos extends Component {
	static propTypes = {
		loadPhotos: PropTypes.func.isRequired,
		photoList: PropTypes.array.isRequired,
		photosPagination: PropTypes.object.isRequired,
	}

  componentWillMount() {
		const {photoList} = this.props
		if (photoList.length === 0) {
			this.props.loadPhotos('/photos')
		}
  }

	// 显示列表
  renderTeaser(item, index) {
		return <PhotoTeaser
						index={index}
						item={item}
						key={item.id}
						/>
  }

	_handleWaypointEnter() {
		const { loadPhotos, pagination } = this.props
		if(typeof pagination.nextPageUrl !== 'undefined') {
			loadPhotos(pagination.nextPageUrl)
		}
	}

	render() {
		const { photoList, loadPhotos, photosPagination } = this.props
		const title = '图片-' + GLOBAL_TITLE
		return (
			<DocumentTitle title={title}>
				<div className="main inner">
					<PhotoList
						items={photoList}
						renderItem={this.renderTeaser}
					/>

					<Waypoint
						onEnter={this._handleWaypointEnter}
						loadPhotos={loadPhotos}
						pagination={photosPagination}
					/>

					{photosPagination.isFetching ? loading : ''}

				</div>			
			</DocumentTitle>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
  const {
    pagination: { pagedPhotos },
    entities: { photos },
  } = state

	const photosPagination = pagedPhotos['photos'] || {ids: []}
  const photoList = photosPagination.ids.map(id => photos[id])

	return {
		photoList,
		photosPagination
	}
}

export default connect(mapStateToProps, {
	loadPhotos
})(Photos)
