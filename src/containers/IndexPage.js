/*
 * 触屏版的首页
 */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { loadPosts } from '../actions/peniel'
import { getEndPointByLocation, GLOBAL_TITLE } from '../conf/apis'

import PostList from '../components/PostList'
import PostTeaser from '../components/PostTeaser'
import PostTeaserMoreImgs from '../components/PostTeaserMoreImgs'
import PostTeaserBigImage from '../components/PostTeaserBigImage'

import Waypoint from 'react-waypoint'
import DocumentTitle from 'react-document-title'


import { getCategoryIdFromPath, getPaginationKey, loading } from '../utils/helper'

class IndexPage extends Component {
	static propTypes = {
		postsList: PropTypes.array.isRequired,
		postsPagination: PropTypes.object.isRequired,
		loadPosts: PropTypes.func.isRequired
	}

	// 第一次打开页面手动输入url
  componentWillMount() {
		const { postsList, location } = this.props
		const category = getCategoryIdFromPath(location)
		const endpoint = getEndPointByLocation(location)
		if(postsList.length === 0) {
			this.props.loadPosts(category, endpoint)
		}
  }

	// 更新了url
  componentWillReceiveProps(nextProps) {
    const { postsList } = nextProps

		// 如果更新了url并且下一个页面的内容为空则载入新闻
		if (this.props.location !== nextProps.location) {
			const category = getCategoryIdFromPath(nextProps.location)
			if(postsList.length === 0) {
				this.props.loadPosts(category, getEndPointByLocation(nextProps.location))
			}
		}
  }

	// 当页面滚动到底部的时候，加载更多新闻
	_handleWaypointEnter() {
		// trigger load more
		const { isFetching, loadPosts, category, nextPageUrl } = this.props
		// 如果没有载入，载入中显示
		if(isFetching === false) {
			loadPosts(category, nextPageUrl)
		}
	}

	// 显示列表
  renderPostTeaser(post, index) {
		// 如果是第一个文章显示大图
		let teaser
		if(index === 0) {
			teaser = <PostTeaserBigImage
				index={index}
				post={post}
				key={post.id}
      />
		} else {
			const imgsCount = post.imgs.length
			if(imgsCount > 1) {
				teaser = <PostTeaserMoreImgs
					index={index}
					post={post}
					key={post.id}
					text={post.text}
				/>
			} else {
				teaser = <PostTeaser
					index={index}
					post={post}
					key={post.id}
					text={post.text}
				/>
			}
		}

    return teaser
  }

	render() {
    const { postsList, postsPagination, loadPosts, location } = this.props
		const category = getCategoryIdFromPath(location)
		return (
		<DocumentTitle title={GLOBAL_TITLE}>
			<div className="main inner">
				<PostList 
					items={postsList}
					renderItem={this.renderPostTeaser}
				/>
				<Waypoint
					onEnter={this._handleWaypointEnter}
					loadPosts={loadPosts}
					category={category}
					{...postsPagination}
				/>
				{postsPagination.isFetching ? loading : ''}
			</div>			
		</DocumentTitle>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
  const {
    pagination: { pagedPosts },
    entities: { posts },
  } = state

	const postsKey = getPaginationKey(ownProps.location)
	const postsPagination = pagedPosts[postsKey] || {ids: []}
  const postsList = postsPagination.ids.map(id => posts[id])

	return {
		postsList,
		postsPagination
	}
}

export default connect(mapStateToProps, {
	loadPosts
})(IndexPage)
