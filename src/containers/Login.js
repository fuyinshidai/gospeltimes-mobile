import React, { Component } from 'react'
import { API_LOGIN } from '../conf/apis'
import $ from 'jquery'

export default class List extends Component {
  constructor() {
    super();
    this.state = {
      windowObjectReference: false
    }
  }

  static propTypes = {
  }

  static defaultProps = {

  }

	closeLogin(state) {
		$("#loginForm").hide()
	}


	handleLogin(loginType) {
		const urlToOpen = API_LOGIN + '?state=' + window.location.href + '&type=' + loginType
		window.location.href = urlToOpen
	}


  render() {
    return (
      <div id="loginForm" style={{display: 'none'}}>
					<div className="mhome mainLogin">
						<div className="mainbg clearfix">
							<div className="return" onClick={() => this.closeLogin()}><img src="/img/index_38_03.png" title="返回上一页" alt="" /></div>
							<div className="logoA clearfix"><img src="/img/index_37_03.png" alt="" /></div>
							<div className="login clearfix">
									<h3>用社交账号直接登录</h3>
									<ul>
										<li><a onClick={() => this.handleLogin('qq')}><img src="/img/index_37_07.png" alt="" /></a></li>
										<li><a onClick={() => this.handleLogin('sina_weibo')}><img src="/img/index_37_10.png" alt="" /></a></li>
									</ul>
							</div>
						</div>
					</div>
			</div>
    )
  }
}
