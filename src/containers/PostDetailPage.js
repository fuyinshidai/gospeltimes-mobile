/*
 * 显示文章的详情
 */
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { loadPost, loadRelated, loadComments } from '../actions/peniel'

import RelatedPost from '../components/RelatedPost'
import PostTeaser from '../components/PostTeaser'
import PostTeaserMoreImgs from '../components/PostTeaserMoreImgs'
import Cnzz from '../components/cnzz'
import Comments from '../components/Comments'

import { API_POST_COMMENT, API_POST_COMMENT_LIKE, COMMENT_TYPE_POST, GLOBAL_TITLE } from '../conf/apis'

import Comment from '../containers/Comment'

import { loading, scrollTop } from '../utils/helper'

import DocumentTitle from 'react-document-title'


import $ from 'jquery'
import Waypoint from 'react-waypoint'

// import { DOMProperty } from 'react/lib/ReactInjection'

// DOMProperty.injectDOMPropertyConfig({
//   isCustomAttribute: attributeName => attributeName === 'sid',
// })

class PostDetailPage extends Component {
  constructor(props) {
    super(props)

	this.handleComment = this.handleComment.bind(this)
	this.postComment = this.postComment.bind(this)
	this.showComment = this.showComment.bind(this)
	this.likeComment = this.likeComment.bind(this)
	this.closeComment = this.closeComment.bind(this)
	this.loadMoreComments = this.loadMoreComments.bind(this)

    this.state = {
      comment: null,
			commentParent: null
    }
  }

	handleComment(event) {
		this.setState({comment: event.target.value});
	}

	static porpTypes = {
		id: PropTypes.number.isRequired,
		post: PropTypes.object.isRequired,
		loadPost: PropTypes.func.isRequired,
		loadRelated: PropTypes.func.isRequired,
		related: PropTypes.array.isRequired,
		relatedData: PropTypes.array.isRequired,
		comments: PropTypes.array.isRequired,
		commentsPagination: PropTypes.array.isRequired
	}

	componentWillMount() {
		// 首次打开页面载入文章
		const { id, post, loadPost } = this.props
		if(Object.keys(post).length === 0 && post.constructor === Object) {
			loadPost(id)
		}

		// track,统计文章点击量
		$.get('http://api.gospeltimes.cn/hit?id=' + id, function(data) {
			console.log('hit data', data)
		})



	}

	// 打开文章初始化滚动到页面顶部
	componentDidMount() {
			scrollTop()
  }
	
	// 更新了url
  componentWillReceiveProps(nextProps) {
		// 如果更新了url
		if (this.props.location.pathname !== nextProps.location.pathname) {
			scrollTop()
		}
  }

	createMarkup(content) {
			return {__html: content};
	}

	// 处理打赏按钮
	handlePraise() {
		$(".admireCode").toggle()
	}

	// 加载相关文章
	_handleLoadRelated() {
		const { id, loadRelated, posts } = this.props
		if(posts.length === 0 && id) {
			loadRelated(id)
		}
	}

	/**
	 * 加载文章评论
	 */
	_handleLoadComments() {
		const { id, loadComments, comments } = this.props
		if(comments.length === 0 && id) {
			loadComments(id)
		}
	}


	// 显示文章列表
  renderPostTeaser(post, index) {
		// 如果是第一个文章显示大图
		let teaser

		const imgsCount = post.imgs.length
		if(imgsCount > 1) {
			teaser = <PostTeaserMoreImgs
				index={index}
				post={post}
				key={post.id}
				text={post.text}
			/>
		} else {
			teaser = <PostTeaser
				index={index}
				post={post}
				key={post.id}
				text={post.text}
			/>
		}

    return teaser
  }

	// 打开评论弹出表单
	showComment(commentParent) {
		const { user } = this.props

		if (typeof(user.id) === 'undefined') {
			$('#loginForm').show()
		} else {
			$(".fabu").stop().slideDown();
			$(".topBtn").stop().hide();
			$(".discuss").stop().hide();
			this.setState({commentParent, comment: ''});
		}
	}

	// 给评论点赞
	likeComment(comment, target) {
		const { user } = this.props

		if (typeof(user.id) === 'undefined') {
			$('#loginForm').show()
		} else {

			const { post, user, loadComments } = this.props
			const commentId = comment.id
			const commentData = {
					token: user.token,
					type: user.type,
					commentId
				}
				$.post( API_POST_COMMENT_LIKE, commentData, function() {})
				.done(function(data) {
					if(data.success) {
						loadComments(post.id, null, comment.id)
					}
					window.swal({   title: data.msg,    timer: 1000,   showConfirmButton: false });
				})
			.fail(function() {
			})
			.always(function() {
			});
		}
	}

	// 关闭评论弹出表单
	closeComment() {
		$(".fabu").stop().slideUp();
		$(".topBtn").stop().show();
		$(".discuss").stop().show();
	}


	// 发表评论
	postComment() {
		const { post, user, loadComments, comments } = this.props
		const parentComment = this.state.commentParent;
		const parentId = (parentComment && parentComment.id) || null
		const commentData = {
			attachId: post.id,
			token: user.token,
			type: user.type,
			content: this.state.comment,
			parentId,
			commentType: COMMENT_TYPE_POST
		}
		const that = this
		$.post( API_POST_COMMENT, commentData, function() {})
			.done(function(data) {
				if(data.success) {
					that.closeComment()
					loadComments(post.id, null, parentId)
				}
				window.swal({   
					title: data.msg,
					timer: 1000,
					showConfirmButton: false 
				});
			})
			.fail(function() {
			})
			.always(function() {
			});
	}

	loadMoreComments() {
    const { post, comments, loadComments } = this.props

		if(comments.length) {
			let last = comments.slice(-1)[0] 
			loadComments(post.id, last.id)
		}
	}


	render() {
    const { post, loadRelated, loadComments, related, relatedData, comments, commentsPagination } = this.props

		const styles= {
			thumb: {
				'margin': '0 auto',
				'display': 'none'
			}
		}

		const images = post.imgs || []
		let imgs = images.map((img, i) => {
										return (
											<div key={i} style={styles.imageWrapper}>
												<img style={styles.img} src={img.urlDetailNew} alt={img.alt} />
												<span style={styles.caption}>{img.alt}</span>
											</div>
											)
							})
		if(images.length === 0) {
			imgs = ""
		}

		// 是否显示打赏
		let dashang
		if(post.dashang === 1) {
			dashang = <div className="admire clearfix" style={styles.showDashang}>
					<div className="admireBtn">								
						<input type="button" name="codeBtn" className="codeBtn" id="codeBtn" onClick={this.handlePraise} value="打赏" />
						<span>如有共鸣和感动，欢迎赞赏支持我们！</span>
						<div className="admireCode" style={styles.codeDisplay}>
							<i></i>
							<img alt="" src="http://www.gospeltimes.cn/themes/fuyinshibao/Public/images/code_1.png" />
						</div>
					</div>								
				</div>
		}

		const title = post.title ? post.title : GLOBAL_TITLE


		return <DocumentTitle title={title}>
			<div>
				<div className="main container clearfix">
					<div id='wx_pic' style={styles.thumb}>
						<img src={post.thumb} alt="" />
					</div>
					<div className="headline clearfix">
						<h1>{post.title}</h1>
						<div className="data clearfix">
							<span>{post.author}</span>
							<span>来源：{post.comeFrom}</span>
							<span>{post.postDate}</span>
						</div>
					</div>

					<div className="article clearfix">
						{ imgs }
					</div>

					<div className="article clearfix">
						<div dangerouslySetInnerHTML={this.createMarkup(post.content)}></div>
					</div>
					<div className="article clearfix">
                                            <a href="https://item.taobao.com/item.htm?id=576816350694">
                                            <img src="/img/toy.jpg" alt="基督教儿童圣经故事播放器早教机主日学玩具"/>
                                            </a>
					</div>

					{dashang}


				</div>

				{/*动态加载评论*/}
				{/* <Waypoint
					onEnter={this._handleLoadComments}
					loadComments={loadComments}
					comments={comments}
					id={post.id}
				/> */}

				{/*评论列表*/}
				{/* <Comments 
					items={ comments }
					showComment={ this.showComment }
					likeComment={ this.likeComment }
					loadMore={ this.loadMoreComments }
					commentsPagination={commentsPagination}
				/>
				{commentsPagination.isFetching ? loading : ''} */}



				{/*动态加载相关文章*/}
				<Waypoint
					onEnter={this._handleLoadRelated}
					loadRelated={loadRelated}
					posts={ related }
					id={post.id}
				/>

				{relatedData.isFetching ? loading : ''}
				<RelatedPost
					posts={ related }
					renderItem={this.renderPostTeaser}
				/>

				{/*页面底部头像评论栏*/}
				{/* <Comment
					showComment={ this.showComment }
			 	/> */}

				{/*评论弹出表单*/}
				 {/* <div className="fabu clearfix">
					 <div className="fabu-btn clearfix">
						 <a className="fl cancelPl"  onClick={() => this.closeComment(this)}>取消</a>
						 <a className="fr publish"   onClick={() => this.postComment()}>发布</a>
					 </div>
					 <textarea className="fabu-nr" id="fabuText" name="fabu-nr" placeholder="我来说两句..." onChange={this.handleComment}>{this.state.comment}</textarea>
				 </div>	 */}

				<Cnzz />
			</div>
		</DocumentTitle>
	}
}

const mapStateToProps = (state, ownProps) => {
	const id = ownProps.params.id
	const post = state.entities.posts[id] || {}
	const relatedKey = 'post-' + id
	const relatedData = (
			state.related && 
			state.related.relatedPosts[relatedKey]
			) || {}
	const related = (
			state.related && 
			state.related.relatedPosts[relatedKey] && 
			state.related.relatedPosts[relatedKey].ids.map(id => state.entities.posts[id])
			) || []

	const commentKey = 'post-' + post.id
	const commentsPagination = state.pagination.comments[commentKey] || []
	let comments = (
			state.pagination.comments[commentKey] && 
			state.pagination.comments[commentKey].ids.map(id => state.entities.comments[id])
			) || []
	// 按照评论id由大到小排序
	comments.sort((a, b) => a.id > b.id ? -1 : 1)


	const user = state.entities.user || {}
	return {
		id,
		post,
		related,
		relatedData,
		comments,
		commentsPagination,
		user
	}
}

export default connect(mapStateToProps, {
	loadPost,
	loadRelated,
	loadComments
})(PostDetailPage)
