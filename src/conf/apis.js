/*
 * api请求的相关配置
 */

/**
 * 用户登录有效期
 */
export const LOGIN_EXPIRE = 3600 * 24 * 7

export const API_ROOT = 'http://api.gospeltimes.cn'
export const API_PORT = '7123'

/**
 * 发布评论接口
 * 使用post方式
 */
export const API_POST_COMMENT = API_ROOT + ':' + API_PORT + '/api/comments.json'

/**
 * 加载评论接口
 * 使用get方式
 */
export const API_GET_COMMENT = API_POST_COMMENT

export const API_POST_COMMENT_LIKE = API_ROOT + ':' + API_PORT + '/api/commentlikes.json'

export const API_SEARCH = API_ROOT + ':' + API_PORT + '/api/search.json'

export const SOURCE_SITE_ROOT = 'http://www.gospeltimes.cn'

export const GLOBAL_TITLE = '福音时报触屏版'

/**
 * endpoints of apis
 */
export const ENDPOINT_HOMEPAGE = '/news?category=homepage'

export const API_LOGIN = API_ROOT + ':' + API_PORT + '/user/login'

export const get_endpoint_topic = (id) => {
	return '/topic?id=' + id
}

export const get_endpoint_photo = (id) => {
	return '/photo?id=' + id
}

export const get_endpoint_user_profile = () => {
	return API_ROOT + ':' + API_PORT + '/user/profile'
}

export const getEndPointByLocation = (location) => {

	let result = location.pathname
	let endpoint
	if(result === '/') {
		endpoint = '/news?category=homepage'
	} else if (result.match(/category\/(\d*)/)) {
		result = result.match(/category\/(\d*)/)
		endpoint = '/news?category=' + result[1]
	} else if (result.match(/province\/(\d*)/)) {
		result = result.match(/province\/(\d*)/)
		endpoint = '/news?province=' + result[1]
	}else if (result.match(/topics/)) {
		result = result.match(/topics/)
		endpoint = '/topics'
	}else if (result.match(/topic/)) {
		result = result.match(/topic/)
		endpoint = '/topic'
	}else if (result.match(/search/)) {
		result = result.match(/search/)
		endpoint = '/news?keyword='
	}

	return endpoint
}

// comment types
export const COMMENT_TYPE_POST = 'post'
