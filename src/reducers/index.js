import * as ActionTypes from '../actions'
import * as PenielActionTypes from '../actions/peniel'
import merge from 'lodash/merge'
import paginate from './paginate'
import { routerReducer as routing } from 'react-router-redux'
import { combineReducers } from 'redux'
import swip from './swip'

// Updates an entity cache in response to any action with response.entities.
const entities = (state = { posts: {}, topics: {}, photos: {}, user: {}, keyword: null }, action) => {
	console.log('action.type', action.type)
	if(action.type === PenielActionTypes.UPDATE_USER) {
		const user = {user: action.user}
		return merge({}, state, user)
	}

	if(action.type === PenielActionTypes.UPDATE_KEYWORD) {

		console.log('action.type', action.type)
		const keyword = {keyword: action.keyword}
		return merge({}, state, keyword)
	}

  if (action.response && action.response.entities) {
    return merge({}, state, action.response.entities)
  }

  return state
}

// Updates error message to notify about the failed fetches.
const errorMessage = (state = null, action) => {
  const { type, error } = action

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null
  } else if (error) {
    return action.error
  }

  return state
}

// Updates the pagination data for different actions.
const pagination = combineReducers({
  pagedPosts: paginate({
    mapActionToKey: action => action.posts,
    types: [
			PenielActionTypes.POSTS_REQUEST,
      PenielActionTypes.POSTS_SUCCESS,
      PenielActionTypes.POSTS_FAILURE
    ]
  }),
  pagedTopics: paginate({
    mapActionToKey: action => action.topics,
    types: [
			PenielActionTypes.TOPICS_REQUEST,
      PenielActionTypes.TOPICS_SUCCESS,
      PenielActionTypes.TOPICS_FAILURE
    ]
  }),

  pagedPhotos: paginate({
    mapActionToKey: action => action.key,
    types: [
			PenielActionTypes.PHOTOS_REQUEST,
      PenielActionTypes.PHOTOS_SUCCESS,
      PenielActionTypes.PHOTOS_FAILURE
    ]
  }),

  topicList: paginate({
    mapActionToKey: action => action.key,
    types: [
			PenielActionTypes.TOPIC_REQUEST,
      PenielActionTypes.TOPIC_SUCCESS,
      PenielActionTypes.TOPIC_FAILURE
    ]
  }),

  comments: paginate({
    mapActionToKey: action => action.key,
    types: [
			PenielActionTypes.COMMENT_REQUEST,
      PenielActionTypes.COMMENT_SUCCESS,
      PenielActionTypes.COMMENT_FAILURE
    ]
  }),

  pagedSearch: paginate({
    mapActionToKey: action => action.key,
    types: [
	  PenielActionTypes.SEARCH_REQUEST,
	  PenielActionTypes.SEARCH_SUCCESS,
	  PenielActionTypes.SEARCH_FAILURE
    ]
  }),
})

// update related posts
const related = combineReducers({
  relatedPosts: paginate({
    mapActionToKey: action => action.post,
    types: [
			PenielActionTypes.RELATED_POST_REQUEST,
      PenielActionTypes.RELATED_POST_SUCCESS,
      PenielActionTypes.RELATED_POST_FAILURE
    ]
  }),
})


const rootReducer = combineReducers({
  entities,
  pagination,
	related,
  errorMessage,
	swip,
  routing
})

export default rootReducer
