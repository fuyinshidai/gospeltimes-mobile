import * as ActionTypes from '../actions/peniel'
import Swiper from 'swiper'

const swiper = () => {
	return new Swiper('.swiper-container', {
		spaceBetween: 10,  //slide之间的距离
		centeredSlides: false,  //设定为true时，活动块会居中，默认情况下居左
		slidesPerView: '5',  //设置slider容器能够同时显示的slides数量
		touchRatio: 0.2,  //触摸距离与slide滑动距离的比率
		slideToClickedSlide: true,  //设置为true则swiping时点击slide会过渡到这个slide
		freeMode:true,  //默认为false，普通模式：slide滑动时只滑动一格，并自动贴合wrapper，设置为true则变为free模式，slide会根据惯性滑动且不会贴合
		observer:true,  //修改swiper自己或子元素时，自动初始化swiper
		observeParents:true  //修改swiper的父元素时，自动初始化swiper
	})
}
const swip = (state = { swiper,  currentSwiper: null}, action) => {
	if(action.type === ActionTypes.UPDATE_SWIPER_INSTANCE) {
		return {
			...state,
			currentSwiper: swiper()
		}
	}
	return state
}

export default swip
