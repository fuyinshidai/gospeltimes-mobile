import React from 'react'
import { Link } from 'react-router'
import $ from 'jquery'

/**
 * 根据当前的路径获取分类id
 */
export const getCategoryIdFromPath = (location) => {
	let result = location.pathname
	if(result === '/') {
		result = 'homepage'
	} else if(result.match(/category\/(\d*)/)) {
		result = result.match(/category\/(\d*)/)
		result = result[1]
	}  else if(result.match(/province\/(\d*)/)) {
		result = result.match(/province\/(\d*)/)
		result = result[1]
	}

	return result
}

export const getPaginationKey = (location) => {
	return 'category-' + getCategoryIdFromPath(location)
}

export const showCategory = (post) => {
	if(typeof post.cateType !== 'undefined') {
		if(post.cateType === "1") {
			return <div className="tag site-tag clearfix"><span><Link to={'/province/' + post.province.categoryId}>{post.province.name}</Link></span></div>
		} 
		if(post.cateType === "2") {
			return <div className="tag topic-tag clearfix"><span><Link to={'/category/'+post.term.termId}>{post.term.name}</Link></span></div>
		}
	}

	if(typeof post.areaId !== 'undefined') {
			return <div className="tag site-tag clearfix"><span><Link to={'/province/' + post.areaId}>{post.areaName}</Link></span></div>
	}
}

export const link_post = (post) => {
	return <Link to={'/news/'+post.id}>{post.title}</Link>
}

export const loading = <div className="dropload-load"><span className="loading"></span>加载中...</div>

export const scrollTop = () => {
	$("html, body").animate({"scrollTop":"0px"},500);
}

