/*
 * 专题teaser
 */
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

export default class TopicTeaser extends Component {
	static defaultProps = {
		item: PropTypes.object.isRequired,
		key: PropTypes.number.isRequired,
	}

	render() {
		const { item } = this.props
		const url = '/photo/' + item.id
		return (
			<article className="clearfix">
				<div className="news-list clearfix">
						<h1 className="news-h1"><Link to={url}>{item.title}</Link></h1>
			    	<div className="news-imga clearfix">
			       	<Link to={url}><img src={item.cover} alt={item.title} /></Link>
			      </div>
				</div>
			</article>
		)
	}
}
