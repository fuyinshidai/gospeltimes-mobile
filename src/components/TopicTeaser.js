/*
 * 专题teaser
 */
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

export default class TopicTeaser extends Component {
	static defaultProps = {
		item: PropTypes.object.isRequired,
		key: PropTypes.number.isRequired,
	}

	render() {
		const { item } = this.props
		const img = 'http://www.gospeltimes.cn/data/upload/' + item.topic
		const url = '/topic/' + item.id
		return (
			<article className="clearfix">
				<div className="news-list clearfix">
					<div className="news-listb fl">
						<h1 className="news-h1"><Link to={url}>{item.title}</Link></h1>
						<div className="time clearfix"><span>{item.postDate}</span></div>
					</div>
					<div className="news-imgb fr">
						<Link to={url}><img src={img} alt={item.title} /></Link>
					</div>
				</div>
			</article>
		)
	}
}
