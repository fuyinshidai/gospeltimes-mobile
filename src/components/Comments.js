/*
 * 评论列表的wrapper
 * 显示评论标题，加载更多按钮
 * 不包括具体的评论显示，
 * 具体的评论显示通过CommentList模块进行展示
 */
import React, { Component, PropTypes } from 'react'

import CommentList from './CommentList'

export default class Comments extends Component {
  static propTypes = {
		items: PropTypes.array.isRequired,
		showComment: PropTypes.func.isRequired,
		likeComment: PropTypes.func.isRequired,
		loadMore: PropTypes.func.isRequired,
		commentsPagination: PropTypes.object.isRequired,
  }



  render() {
    const {
      items, showComment, likeComment, commentsPagination
    } = this.props

		let moreText
		if(items.length === 0) {
			moreText = '还没有评论。'
		} else {
			moreText = ''
		}

		const showMore = commentsPagination.nextPageUrl ?  <a onClick={() => this.props.loadMore()}>更多评论<span>∨</span></a>
				 : <span>{moreText}</span>

    return (
<div className="comment clearfix">
			<div className="com-h1 clearfix">
				<i></i>
				<h1>精选评论</h1>
			</div>
			<div className="container">
				<CommentList 
						items={ items }
						showComment={ showComment }
						likeComment={ likeComment }
				/>
			</div>
			<div className="moreBtn clearfix">
				{showMore}
			</div>
	   </div>
    )
  }
}
