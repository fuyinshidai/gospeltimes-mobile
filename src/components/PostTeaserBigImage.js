/*
 * 列表第一个大图
 */
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { link_post } from '../utils/helper'

export default class PostTeaserBigImage extends Component {

  static defaultProps = {
		post: PropTypes.object.isRequired,
		key: PropTypes.number.isRequired
  }


  render() {
    const {
			post
    } = this.props
		const url = "/news/" + post.id

    return (
				<article className="clearfix">
			    	<div className="news-list clearfix">
			    		<h1 className="news-h1">{link_post(post)}</h1>
			            <div className="time clearfix"><span>{post.post_date}</span></div>
			            <div className="news-imga clearfix">
										<Link to={url}><img src={post.thumb} alt={post.alt} /></Link>
			            </div>
			    	</div> 
			    </article>
    )
  }
}
