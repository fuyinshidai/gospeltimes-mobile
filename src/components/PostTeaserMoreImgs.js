/*
 * 新闻列表的新闻单个显示
 * 显示新闻的标题、简介、作者、时间等信息
 */
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

import { showCategory, link_post } from '../utils/helper'

export default class PostTeaser extends Component {

  static defaultProps = {
		post: PropTypes.object.isRequired,
		key: PropTypes.number.isRequired
  }


  render() {
    const { post } = this.props
		const url = "/news/" + post.id

		const imgs = <div className="news-imgc clearfix">
							{post.imgs.map((i, index) => index < 3 ? <Link key={index} to={url}><img src={i.urlNew} alt={post.alt} /></Link> : null)}
						</div>


    return (
			    <article className="clearfix">
			    	<div className="news-list clearfix">
							{showCategory(post)}
			    		<h1 className="news-h1">{link_post(post)}</h1>
							{imgs}
			        <div className="time clearfix"><span>{post.post_date}</span></div>
			    	</div>
			    </article>
    )
  }
}
