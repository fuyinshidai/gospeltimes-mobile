/*
 * 显示评论的具体内容列表
 * -评论点赞
 *  -评论回复
 */
import React, { Component, PropTypes } from 'react'

export default class Comments extends Component {
	static propTypes = {
		items: PropTypes.array.isRequired,
		showComment: PropTypes.func.isRequired,
		likeComment: PropTypes.func.isRequired
	}

	static defaultProps = {
	}

	// 计算评论下面的子评论
	countComments(comment) {
		let count = comment.children.length
			let that = this
			if(comment.children.length > 0) {
				comment.children.forEach((v) => {
					count += that.countComments(v)
				})
			}
		return count
	}


	render() {
		const {
			items, renderItem, likeComment
		} = this.props

		console.log('this.props', this.props)
		return (
				<div className="comment clearfix">
					<div className="container">
						{items.map((comment, key) => {
							return	<div key={key} className="com-pl clearfix">
										<div className="head fl">
											<img src={comment.user.avatar} alt="" />
										</div>
										<div className="comment-count fl">
											<div className="usename clearfix">
												<h3 className="name-one fl">{comment.user.nickname}<span>{comment.time}</span></h3>
												<div className="useBol fr">
													{/*用户点赞按钮*/}
													<div className="use-ly zhan fl">
														<span>{comment.likeCount}</span>
														<img src="/img/article_23.png" className="btn-like" alt="like" onClick={() => likeComment(comment, this)} />
													</div>
													{/*评论回复按钮*/}
													<div className="use-ly liuyan  fl">
														<span>{this.countComments(comment)}</span>
														<img src="/img/article_29.png" alt=""  onClick={() => this.props.showComment(comment)}/>
													</div>
												</div>
											</div>
											<div className="message clearfix">
												<div className="message-one">
													<p>{comment.content}</p>
													<Comments 
														items={ comment.children }
														showComment={ this.props.showComment }
														likeComment={ likeComment }
													/>
												</div>
											</div>
										</div>
									</div>
						})}
					</div>
				</div>
			)
	}
}
