import React from 'react'

const RelatedPost = ({ posts, renderItem }) => {
	if(Object.keys(posts).length === 0) {
		return <div />
	}
	if(posts) {
		return (
		<div className="comment comment-jc clearfix">
			<div className="com-h1 red-h clearfix">
				<i></i>
				<h1>精彩推荐</h1>
			</div>
			{posts.map(renderItem)}
	    </div>	
		)
	}
}

RelatedPost.propTypes = {
}

export default RelatedPost
