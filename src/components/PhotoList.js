import React, { Component, PropTypes } from 'react'

export default class PhotoList extends Component {
	static propTypes = {
		items: PropTypes.array.isRequired,
		renderItem: PropTypes.func.isRequired
	}

	render() {
		const { items, renderItem } = this.props
		return (
			<div className="lists">
				{items.map(renderItem)}
			</div>
		)
	}

}
