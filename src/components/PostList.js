import React, { Component, PropTypes } from 'react'

export default class PostList extends Component {
  static propTypes = {
		items: PropTypes.array.isRequired,
    renderItem: PropTypes.func.isRequired
  }

  static defaultProps = {
  }

	// 更新了url
  componentWillReceiveProps(nextProps) {
  }

	componentDidMount() {
	}


  render() {
    const {
      items, renderItem
    } = this.props

    return (
      <div>
				<div className="lists">
					{items.map(renderItem)}
				</div>
      </div>
    )
  }
}
