import React, { Component, PropTypes } from 'react'

export default class PhotoItems extends Component {
	static propTypes = {

		items: PropTypes.array.isRequired,
		renderItem: PropTypes.func.isRequired
	}

	render() {
		const { items, renderItem } = this.props
		return (
			<div className="my-gallery clearfix" data-pswp-uid="1">
				{items.map(renderItem)}
			</div>
		)
	}
}
