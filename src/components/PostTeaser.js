/*
 * 新闻列表的新闻单个显示
 * 显示新闻的标题、简介、作者、时间等信息
 */
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { showCategory, link_post } from '../utils/helper'

export default class PostTeaser extends Component {

  static defaultProps = {
		post: PropTypes.object.isRequired,
		key: PropTypes.number.isRequired
  }


  render() {
    const {
			post
    } = this.props
		const url = "/news/" + post.id

		let img
		let className = 'news-listb fl'
		if(post.imgs.length === 0 ) {
			if(post.thumbS) {
				className = 'news-listb fl'
				img = <Link to={url}><img src={post.thumbS} alt={post.alt} /></Link>
			} else {
				className = ''
			}
		} else {
			className = 'news-listb fl'
			img = post.imgs.map((img) => <Link key={img} to={url}><img src={img.urlNew} alt={post.alt} /></Link>)
		}

    return (
			    <article className="clearfix">
			    	<div className="news-list clearfix">
							{showCategory(post)}
			    		<div className={className}>
			    			<h1 className="news-h1">{link_post(post)}</h1>
				    		<div className="time clearfix"><span>{post.post_date}</span></div>
			    		</div>
			    		<div className="news-imgb fr">
							{img}
			            </div>
			    	</div>
			    </article>
    )
  }
}
